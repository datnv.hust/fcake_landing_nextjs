# facke landing page

## How to run **facke**

- Install Mysql
- In `/facke/backend` run `npm install`
- In `/facke/frontend` run `npm install`
- In `/facke`, create .env file, edit config then run `npm run develop`

*Example:*

```bash
cd ./facke/backend
npm install
```
```bash
cd ./facke/backend
npm install
```
```bash
cd ./facke
npm install
npm run develop
```


- Go to backend `http://localhost:1337/admin`
- Go to frontend `http://localhost:3000`

*Example run mysql with docker-compose file:*
- check `\.dev` folder