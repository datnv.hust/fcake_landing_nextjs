const { sanitizeEntity } = require('strapi-utils');
/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  /**
* Retrieve a record.
*
* @return {Object}
*/
  async find(ctx) {
    let entities;
    [entities, total] = await Promise.all([
      strapi.services.blogs.find(ctx.query),
      strapi.services.blogs.count(ctx.query),
    ]);
    return { 
      blog_list: sanitizeEntity(entities, { model: strapi.models.blogs, includeFields: ["title", "description", "poster_name", "poster_avatar", "slug", "publish_at", "category", "comment_count", "background_image"] }),
      total: total,
    };
  },

  async findOne(ctx) {
    const { slug } = ctx.params;
    const entity = await strapi.services.blogs.findOne({ slug });
    return sanitizeEntity(entity, { model: strapi.models.blogs, includeFields: ["title", "description", "poster_name", "poster_avatar", "slug", "publish_at", "category", "comment_count", "background_image", "content"] });

  },

  async findByCategory(ctx) {
    const { slug } = ctx.params;
    let category, blog, total, tags;

    category = await strapi.services.category.findOne({ slug })

    if (!category) {
      ctx.throw(404)
    }

    [blog, total, tags] = await Promise.all([
      strapi.services.blogs.find({ "category.slug": slug, ...ctx.query }),
      strapi.services.blogs.count({ "category.slug": slug, ...ctx.query }),
      strapi.services.tags.find({ "blogs.category.slug": slug }),
    ]);
    return {
      total: total,
      tags: sanitizeEntity(tags, { model: strapi.models.tags, includeFields: ["name", "slug"] }),
      blog_list: sanitizeEntity(blog, { model: strapi.models.blogs, includeFields: ["title", "description", "poster_name", "poster_avatar", "slug", "publish_at", "category", "comment_count", "background_image"] }),
      category: sanitizeEntity(category, { model: strapi.models.category, includeFields: ["name", "slug"] }),
    }
  },
  async findByTag(ctx) {
    const { slug } = ctx.params;
    let tag, blog, total, tags;

    tag = await strapi.services.tags.findOne({ slug })

    if (!tag) {
      ctx.throw(404)
    }

    [blog, total, tags] = await Promise.all([
      strapi.services.blogs.find({ "tags.slug": [slug], ...ctx.query }),
      strapi.services.blogs.count({ "tags.slug": [slug], ...ctx.query }),
      strapi.services.tags.find(),
    ]);
    return {
      total: total,
      tags: sanitizeEntity(tags, { model: strapi.models.tags, includeFields: ["name", "slug"] }),
      blog_list: sanitizeEntity(blog, { model: strapi.models.blogs, includeFields: ["title", "description", "poster_name", "poster_avatar", "slug", "publish_at", "category", "comment_count", "background_image"] }),
      tag: sanitizeEntity(tag, { model: strapi.models.tags, includeFields: ["name", "slug"] }),
    }
  },
};
