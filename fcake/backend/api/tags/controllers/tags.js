const { sanitizeEntity } = require('strapi-utils');

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  /**
  * Retrieve records.
  *
  * @return {Array}
  */

  async find(ctx) {
    let entities;
    entities = await strapi.services.tags.find(ctx.query);
    return sanitizeEntity(entities, { model: strapi.models.tags, includeFields: ["name", "slug"] });
  },
};
