import Link from "next/link";
import React from "react";

const BannerTitle = ({ title = "Blogs", path = [] }) => {
  return (
    <div className="ld-breadcrumb bg-purple d-flex align-items-center text-white">
      <div className="row mx-auto container">
        <div className="ld-breadcrumb-title col-lg-6">
          <h2>{title}</h2>
        </div>
        <div className="ld-breadcrumb-path d-sm-flex justify-content-end align-items-center col-8 col-lg-6">
          <ul className="mt-sm-0 mt-3">
            <li className="li-post-new">
              <Link href="/">
                <a>
                  <i className="fa fa-home" aria-hidden="true" /> Home
                </a>
              </Link>
            </li>
            {path?.map((x) =>
              x.isActive ? (
                <li className="active"> {x.name}</li>
              ) : (
                <li className="li-post-new">
                  <Link href={x.router}>
                    <a> / {x.name}</a>
                  </Link>
                </li>
              )
            )}
          </ul>
        </div>
      </div>
    </div>
  );
};

export default BannerTitle;
