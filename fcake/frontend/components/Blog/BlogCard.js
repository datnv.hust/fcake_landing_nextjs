import React from "react";
import { getStrapiMedia } from "lib/media";
import { formatTime } from "lib/utils";
import Link from "next/link";
const BlogCard = ({ data }) => {
  const {
    title = "",
    slug = "",
    poster_name = "",
    comment_count = "",
    description = "",
    updated_at = "",
    category = {},
    background_image = {},
  } = data;
  console.log('category :>> ', category, data);
  const link = `/blog/${category?.slug}/${slug}`;
  return (
    <div className="blog-box">
      <div className="blog-post-image">
        <img
          src={getStrapiMedia(background_image)}
          alt="background_blog_post"
        />
      </div>
      <div className="blog-post-detail">
        <div className="meta">
          <ul className="list-icon-meta">
            <li className="icon-meta">
              <i className="far fa-user" aria-hidden="true" />
              {poster_name}
            </li>
            <li className="icon-meta">
              <i className="far fa-calendar-minus" aria-hidden="true" />
              {formatTime(updated_at)}
            </li>
            <li className="icon-meta">
              <i className="far fa-comments" aria-hidden="true" />
              {comment_count}
            </li>
          </ul>
        </div>
        <div className="title-post">
          <h5>
            <Link href={link}>
              <a>{title}</a>
            </Link>
          </h5>
        </div>
        <div className="content-post">
          <p>{description}</p>
        </div>
        <div className="button-post">
          <Link href={link}>
            <a>
              Xem thêm <i className="fa fa-angle-right" aria-hidden="true" />
            </a>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default BlogCard;
