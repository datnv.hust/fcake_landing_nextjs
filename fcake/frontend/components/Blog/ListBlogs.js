import React, { useEffect, useState } from "react";
import BlogCard from "./BlogCard";
import RightListBlog from "./RightListBlog";
import { useRouter } from "next/router";
import { getAsync } from "lib/request";

const ListBlogs = ({ listData = [], total }) => {
  const blogNewUpdate = listData.slice(0, 5);
  const [currentPageNumber, setCurrentPageNumber] = useState(0);
  const [listBlogData, setListBlogData] = useState(listData || []);
  const router = useRouter();
  const categorySlug = router?.query?.category;
  const slugTag = router?.query?.tags;
  const fullData = listBlogData?.length === total;

  useEffect(() => {
    setListBlogData(listData);
    setCurrentPageNumber(0);
  }, [listData]);
  useEffect(() => {
    if (currentPageNumber !== 0) {
      handleLoadingData();
    }
  }, [currentPageNumber]); // eslint-disable-line react-hooks/exhaustive-deps
  const handleLoadingData = async () => {
    const dataLoading = await getAsync("/api/load-blog", {
      page: currentPageNumber,
      category: categorySlug,
      tag: slugTag,
    });
    const newsData = dataLoading.data.data || [];
    setListBlogData((x) => [...x, ...newsData]);
  };
  const handleNextCurrentpage = () => {
    if (fullData) {
      return;
    }
    setCurrentPageNumber((x) => x + 1);
  };
  return (
    <div className="wrapper-page container">
      <div className="row">
        <div className="col-md-8 col-sm-12  px-md-3" style={{ minHeight: 300 }}>
          <div className="blog-post-wrap">
            {listBlogData.map((x) => {
              return <BlogCard key={x.id} data={x} />;
            })}
            {!fullData && (
              <div
                onClick={() => handleNextCurrentpage()}
                className="button_load_more"
              >
                Xem thêm
              </div>
            )}
          </div>
        </div>
        <RightListBlog blogNewUpdate={blogNewUpdate} />
      </div>
    </div>
  );
};

export default ListBlogs;
