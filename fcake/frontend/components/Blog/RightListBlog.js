import React from "react";
import Link from "next/link";
import { useGlobalContext } from "pages/_app";
const RightListBlog = ({ blogNewUpdate }) => {
  const { categories, tags } = useGlobalContext();
  const CardBlogNew = ({ data }) => {
    const categorySlug = data?.category?.slug;
    const blogTitle = data?.slug;
    const link = `/blog/${categorySlug}/${blogTitle}`;
    const title = data?.title;
    return (
      <li className="li-post-new">
        <Link href={link}>
          <a> {title}</a>
        </Link>
      </li>
    );
  };
  return (
    <div className="col-md-4 col-sm-12  px-3">
      <div className="recent-post card-small">
        <h2>Bài viết gần đây</h2>
        <ul>
          {blogNewUpdate.map((x) => (
            <CardBlogNew key={x.id} data={x} />
          ))}
        </ul>
      </div>
      <div className="category-post card-small">
        <h2>Danh mục</h2>
        <ul>
          {categories.map((x) => (
            <li className="li-post-new" key={x.id}>
              <Link href={"/blog/" + x.slug}>
                <a>{x.name}</a>
              </Link>
            </li>
          ))}
        </ul>
      </div>
      <div className="tag-blog card-small">
        <h2>Chủ đề nổi bật</h2>
        <div className="list-tag">
          {tags.map((x) => (
            <Link key={x.id} href={"/blog?tags=" + x.slug}>
              <a>{x.name}</a>
            </Link>
          ))}
        </div>
      </div>
    </div>
  );
};

export default RightListBlog;
