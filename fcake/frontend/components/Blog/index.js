import React from "react";
import BannerTitle from "./BannerTitle";
import ListBlogs from "./ListBlogs";

const BlogComponent = ({ data, title, path, total }) => {
  return (
    <>
      <BannerTitle title={title} path={path} />
      <ListBlogs listData={data || []} total={total} />
    </>
  );
};

export default BlogComponent;
