import React from "react";
import RightListBlog from "components/Blog/RightListBlog";
import { getStrapiMedia } from "lib/media";
import { formatTime } from "lib/utils";
import MarkdownContent from "./MarkdownContent";

const ContentBlog = ({ contentData, blogNewUpdate }) => {
  const {
    title = "",
    slug = "",
    poster_name = "",
    comment_count = "",
    description = "",
    updated_at = "",
    category = {},
    background_image = {},
    content,
  } = contentData;
  return (
    <div className="container">
      <div className="row">
        <div className="col-md-8 col-sm-12">
          <article
            id="post-1160"
            className="post-1160 post  status-publish   hentry category-huong-dan"
          >
            <div className="iq-blog-box">
              <div className="iq-blog-image">
                <img
                  width={750}
                  height={422}
                  src={getStrapiMedia(background_image)}
                  className="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                  alt="background_post"
                  loading="lazy"
                  sizes="100vw"
                />
              </div>
              <div className="iq-blog-detail">
                <div className="iq-blog-meta">
                  <ul className="list-inline">
                    <li className="list-inline-item">
                      <i className="far fa-user" aria-hidden="true" />
                      {poster_name}
                    </li>
                    <li className="list-inline-item">
                      <i className="far fa-calendar-minus" aria-hidden="true" />
                      <time
                        className="entry-date published updated"
                        dateTime="2021-05-31T09:52:35+00:00"
                      >
                        {formatTime(updated_at)}
                      </time>
                    </li>
                    <li className="list-inline-item">
                      <i className="far fa-comments" aria-hidden="true" />{" "}
                      {comment_count}
                    </li>
                  </ul>
                </div>
                <div className="blog-content">
                  <MarkdownContent content={content} />
                </div>
              </div>
            </div>
          </article>
        </div>
        <RightListBlog blogNewUpdate={blogNewUpdate} />
      </div>
    </div>
  );
};

export default ContentBlog;
