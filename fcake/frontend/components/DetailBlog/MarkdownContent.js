import { getStrapiURL } from "lib/api";
import ReactMarkdown from "react-markdown";

export default function MarkdownContent({ content }) {
  let replacedContent = content;

  if (content) {
    replacedContent = content.replace(/\/upload/gi, getStrapiURL("/upload"));
  }
  return <ReactMarkdown source={replacedContent} escapeHtml={false} />;
}
