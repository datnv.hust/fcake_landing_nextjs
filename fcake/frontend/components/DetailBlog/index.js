import BannerTitle from "components/Blog/BannerTitle";
import ContentBlog from "./ContentBlog";
import React from "react";

const DetailBlog = ({ detailData, blogNewUpdate }) => {
  const categoryName = detailData?.category?.name;
  const categorySlug = detailData?.category?.slug;
  const title = detailData?.title;
  const path = [
    { id: 1, name: "Blog", router: "/blog", isActive: false },
    {
      id: 2,
      name: categoryName,
      router: `/blog/${categorySlug}`,
      isActive: false,
    },
    { id: 3, name: title, isActive: true },
  ];
  return (
    <>
      <BannerTitle title={title} path={path} />
      <ContentBlog
        contentData={detailData}
        blogNewUpdate={blogNewUpdate?.blog_list || []}
      />
    </>
  );
};

export default DetailBlog;
