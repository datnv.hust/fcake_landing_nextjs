import banner1 from "../../assets/images/index_files/wp-content/uploads/2019/11/Untitled-2.jpg"
import banner2 from "../../assets/images/index_files/banner-img1.png"
import banner3 from "../../assets/images/index_files/banner-img2.png"
import { useEffect, useState } from "react";

const Banner = () => {
  const [show, setShow] = useState(true);
  useEffect(() => {
    const id = setInterval(() => setShow(show => !show), 7000);
    return () => clearInterval(id);
  }, [])

  function setTranslate(e) {
    console.log(`e`, e)
    let x = e.clientX;
    let y = e.clientY;
    document.getElementById("rsLayer").style.transform = `translate3d(-${x * 0.03}px,-${y * 0.03}px,0px)`
  }
  return (
    <div className="banner">
      <div className="banner_background">
        <img src={banner1.src}
          className="banner_background-img" data-bgcolor="transparent" ></img>
      </div>
      <div id="banner_first" class={show ? "banner-box" : "banner-box display-none"} onMouseMove={setTranslate}>
        <div className="banner_first-text">
          <div className="banner_first-large">
            <span>Fcake.vn</span>
            <span>Nền tảng hỗ trợ bán hàng online </span>
            <span>bằng công nghệ AI</span>
          </div>
          <div className="banner_first-efficient">
            <div>Gia tăng doanh số & quản lý khách hàng hiệu quả</div>
            <div>Tăng tỷ lệ chốt đơn hàng thông qua gợi ý khách hàng tiềm
              năng chính xác đến 90%</div>
          </div>
          <a href="https://app.fcake.vn/" className="banner-first__register">Đăng kí ngay</a>
        </div>
        <div id="rsLayer" className="banner-first__img-box animate__animated animate__zoomIn">
          <img src={banner2.src} alt="" className="banner-first__img" />
        </div>
      </div>
      <div id="banner_second" class={show ? "banner-box display-none" : "banner-box"}>
        <div className="banner_first-text">
          <div className="banner_first-large">
            <span>Fcake.vn</span>
            <span>Nền tảng hỗ trợ bán hàng online </span>
            <span>bằng công nghệ AI</span>
          </div>
          <div className="banner_first-efficient">
            <div>Gia tăng doanh số & quản lý khách hàng hiệu quả</div>
            <div>Tăng tỷ lệ chốt đơn hàng thông qua gợi ý khách hàng tiềm
              năng chính xác đến 90%</div>
          </div>
          <a href="#" className="banner-first__register">Đăng kí ngay</a>
        </div>

        <div className="banner-first__img-box --second animate__animated animate__zoomIn">
          <img src={banner3.src} alt="" className="banner-first__img --second" />
        </div>
      </div>
    </div>
  )
}

export default Banner;
