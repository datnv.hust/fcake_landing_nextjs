import image1 from "../../assets/images/index_files/asset-6-min.png"
import image2 from "../../assets/images/index_files/asset-7.png"
import image3 from "../../assets/images/index_files/1-anh-min.png"
import image4 from "../../assets/images/index_files/2-anh.png"
import image5 from "../../assets/images/index_files/3-anh.png"
import image6 from "../../assets/images/index_files/asset-11-min.png"

const Content = () => {
  return (
    <>
      <div className="row wpb_row vc_row-fluid vc_custom_1553861764996">
        <div className="wpb_column vc_column_container col-sm-12">
          <div className="vc_column-inner">
            <div className="wpb_wrapper">
              <div className="row wpb_row vc_inner vc_row-fluid">
                <div className="wpb_column vc_column_container col-sm-12">
                  <div className="vc_column-inner">
                    <div className="wpb_wrapper">
                      <div className="title-box style-two custom-title-home text-center">
                        <h2 className="title">CHỈ 3 BƯỚC ĐỂ FCAKE
                          <span> ĐƯA CỬA HÀNG CỦA BẠN ĐẾN THÀNH
                            CÔNG</span>
                        </h2>
                        <p className="mt-0 mb-0">Fcake sử dụng công nghệ
                          AI để giúp bạn bán hàng và tối ưu nguồn
                          lực</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex">
                <div className="wpb_column vc_column_container col-sm-6" style={{ display: "flex" }}>
                  <div className="vc_column-inner">
                    <div className="wpb_wrapper">
                      <div className="title-box style-two text-left">
                        <h2 className="title">Sử dụng AI <span>Hỗ trợ
                          bán hàng</span></h2>
                        <p className="mt-0 mb-0">Với công nghệ AI, Fcake
                          giúp cửa hàng tối ưu quy trình bán hiệu
                          quả</p>
                      </div>
                      <ul className="iq-list wow fadeInUp" data-wow-duration="0.9s"
                        style={{ visibility: "visible", animationDuration: "0.9s", animationName: "fadeInUp" }}>
                        <li><i className="ion ion-ios-checkmark"></i><span>Quản
                          lý bài viết quảng cáo</span></li>
                        <li><i className="ion ion-ios-checkmark"></i><span>Quản
                          lý livestream bán hàng</span></li>
                        <li><i className="ion ion-ios-checkmark"></i><span>Quản
                          lý livechat và trả lời tự
                          động</span></li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div className="vc_parallax wpb_column vc_column_container col-sm-6">
                  <div className="vc_column-inner">
                    <div className="wpb_wrapper">
                      <div className="works-box pb-5">
                        <img className="img-fluid top-bg"
                          src={image1.src} alt="image" />
                        <img
                          className="img-fluid works-img1" src={image2.src}
                          alt="image" />
                        <img className="img-fluid top-img1 i-size"
                          src={image3.src} alt="image" />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row wpb_row vc_inner vc_row-fluid">
                <div className="wpb_column vc_column_container col-sm-12">
                  <div className="vc_column-inner">
                    <div className="wpb_wrapper">
                      <div className="wpb_single_image wpb_content_element vc_align_center">

                        <figure className="wpb_wrapper vc_figure">
                          <div className="vc_single_image-wrapper vc_box_border_grey">
                          </div>
                        </figure>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="row wpb_row vc_row-fluid vc_custom_1553850167306">
        <div className="wpb_column vc_column_container col-sm-12">
          <div className="vc_column-inner">
            <div className="wpb_wrapper">
              <div className="row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex">
                <div className="wpb_column vc_column_container col-sm-6">
                  <div className="vc_column-inner">
                    <div className="wpb_wrapper">
                      <div className="works-box "><img className="img-fluid top-bg"
                        src={image1.src} alt="image" /><img
                          className="img-fluid works-img1" src={image2.src}
                          alt="image" /><img className="img-fluid top-img1 i-size"
                            src={image4.src} alt="image" />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="wpb_column vc_column_container col-sm-6" style={{ display: "flex" }}>
                  <div className="vc_column-inner">
                    <div className="wpb_wrapper">
                      <div className="title-box style-two text-left">
                        <h2 className="title">Sử dụng AI <span>Quản lý
                          khách hàng</span></h2>
                        <p className="mt-0 mb-0">Fcake cho phép quản lý
                          thông tin khách hàng, tự động phân tích
                          hành vi và phân loại khách hàng tiềm
                          năng chính xác</p>
                      </div>
                      <ul className="iq-list wow fadeInUp" data-wow-duration="0.9s"
                        style={{ visibility: "visible", animationDuration: "0.9s", animationName: "fadeInUp" }}>
                        <li><i className="ion ion-ios-checkmark"></i><span>Quản
                          lý khách hàng</span></li>
                        <li><i className="ion ion-ios-checkmark"></i><span>Fcake
                          - Customer Profiling</span></li>
                        <li><i className="ion ion-ios-checkmark"></i><span>Fcake
                          - Insight</span></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row wpb_row vc_inner vc_row-fluid">
                <div className="wpb_column vc_column_container col-sm-12">
                  <div className="vc_column-inner">
                    <div className="wpb_wrapper">
                      <div className="wpb_single_image wpb_content_element vc_align_center">

                        <figure className="wpb_wrapper vc_figure">
                          <div className="vc_single_image-wrapper vc_box_border_grey">
                          </div>
                        </figure>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="row wpb_row vc_row-fluid vc_custom_1553848524270">
        <div className="wpb_column vc_column_container col-sm-12">
          <div className="vc_column-inner">
            <div className="wpb_wrapper">
              <div className="row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex">
                <div className="wpb_column vc_column_container col-sm-6" style={{ display: "flex" }}>
                  <div className="vc_column-inner">
                    <div className="wpb_wrapper">
                      <div className="title-box style-two text-left">
                        <h2 className="title">Sử dụng AI <span>Chăm sóc
                          khách hàng</span></h2>
                        <p className="mt-0 mb-0">Fcake sẽ hỗ trợ chăm
                          sóc khách hàng tự động và gợi ý sản phẩm
                          phù hợp, tăng tỷ lệ chốt đơn hàng</p>
                      </div>
                      <ul className="iq-list wow fadeInUp" data-wow-duration="0.9s"
                        style={{ visibility: "visible", animationDuration: "0.9s", animationName: "fadeInUp" }}>
                        <li><i className="ion ion-ios-checkmark"></i><span>Tạo
                          mã giảm giá &amp; Chương trình
                          khuyến mãi</span></li>
                        <li><i className="ion ion-ios-checkmark"></i><span>Tạo
                          gaminification</span></li>
                        <li><i className="ion ion-ios-checkmark"></i><span>Gửi
                          tin nhắn hàng loạt</span></li>
                      </ul>
                      <div className="text-left">
                        <a className="button button-icon " href="https://app.fcake.vn/"> Đăng ký
                          ngay</a>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="wpb_column vc_column_container col-sm-6">
                  <div className="vc_column-inner">
                    <div className="wpb_wrapper">
                      <div className="works-box "><img className="img-fluid top-bg"
                        src={image1.src} alt="image" /><img
                          className="img-fluid works-img1" src={image2.src}
                          alt="image" /><img className="img-fluid top-img1 i-size"
                            src={image5.src} alt="image" />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row wpb_row vc_row-fluid vc_custom_1553858734782">
        <div class="wpb_column vc_column_container col-sm-12">
          <div class="vc_column-inner">
            <div class="wpb_wrapper">
              <div class="row wpb_row vc_inner vc_row-fluid no-margin justify-content-md-center">
                <div
                  class="wpb_column vc_column_container col-sm-12 col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8">
                  <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                      <div class="title-box style-two text-center">
                        <h2 class="title">Fcake là con đường ngắn
                          nhất giúp bạn <span>Khai thác &amp; gia
                            tăng chuyển đổi khách hàng</span>
                        </h2>
                        <p class="mt-0 mb-0">Giúp chủ cửa hàng dễ
                          dàng chuyển đổi số trong thời kỳ 4.0. Sử
                          dụng Fcake là sử dụng 1 giải pháp giải
                          quyết tất cả các vấn đề của bạn</p>
                      </div>
                      <div class="text-center">
                        <a class="button button-icon " href="https://app.fcake.vn/"> Sử dụng
                          ngay</a>
                      </div>
                      <div
                        class="wpb_single_image wpb_content_element vc_align_center vc_custom_1624003230119">

                        <figure class="wpb_wrapper vc_figure">
                          <div class="vc_single_image-wrapper vc_box_border_grey">
                            <img width="730" height="469"
                              src={image6.src}
                              class="vc_single_image-img attachment-full" alt="" loading="lazy"
                              sizes="100vw" />
                          </div>
                        </figure>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default Content;
