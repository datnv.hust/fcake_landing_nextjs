import gif from "../../assets/images/index_files/asset-5-min.gif"
import jQuery from "jquery";
import { useEffect } from "react";

const Introduction = () => {
  useEffect(() => {
    let i = 1
    var radius = 200;
    var fields = jQuery('.itemDot');
    var container = jQuery('.dotCircle');
    var width = container.width();
    var loopid = jQuery('.circle_id').attr('id');
    var loop_id = parseInt(loopid) - 1;

    radius = width / 2.5;

    var height = container.height();
    var angle = 0, step = (2 * Math.PI) / fields.length;
    fields.each(function () {
      var x = Math.round(width / 2 + radius * Math.cos(angle) - jQuery(this).width() / 2);
      var y = Math.round(height / 2 + radius * Math.sin(angle) - jQuery(this).height() / 2);


      jQuery(this).css({
        left: x + 'px',
        top: y + 'px'
      });
      angle += step;
    });


    jQuery('.itemDot').click(function () {

      var dataTab = jQuery(this).data("tab");
      jQuery('.itemDot').removeClass('active');
      jQuery(this).addClass('active');
      jQuery('.CirItem').removeClass('active');
      jQuery('.CirItem' + dataTab).addClass('active');
      i = dataTab;

      jQuery('.dotCircle').css({
        "transform": "rotate(" + (360 - (i - 1) * 36) + "deg)",
        "transition": "2s"
      });
      jQuery('.itemDot').css({
        "transform": "rotate(" + ((i - 1) * 36) + "deg)",
        "transition": "1s"
      });


    });

    setInterval(function () {
      var dataTab = jQuery('.itemDot.active').data("tab");

      if (dataTab > loop_id || i > loop_id) {
        dataTab = 0;
        i = 0;
      }
      jQuery('.itemDot').removeClass('active');
      jQuery('[data-tab="' + i + '"]').addClass('active');
      jQuery('.CirItem').removeClass('active');
      jQuery('.CirItem' + i).addClass('active');
      i++;


      jQuery('.dotCircle').css({
        "transform": "rotate(" + (360 - (i - 2) * 36) + "deg)",
        "transition": "2s"
      });
      jQuery('.itemDot').css({
        "transform": "rotate(" + ((i - 2) * 36) + "deg)",
        "transition": "1s"
      });

    }, 5000);
  }, [])
  return (
    <div class="row wpb_row row-fluid">
      <div class="wpb_column vc_column_container col-sm-12 col-lg-6 col-md-4">
        <div class="vc_column-inner">
          <div class="wpb_wrapper">
            <div class="wpb_single_image wpb_content_element vc_align_center radius_white">

              <figure class="wpb_wrapper vc_figure">
                <div class="vc_single_image-wrapper vc_box_border_grey">
                  <img width="1118" height="2244" src={gif.src}
                    class="vc_single_image-img attachment-full" alt="" loading="lazy" sizes="100vw" />
                </div>
              </figure>
            </div>
          </div>
        </div>
      </div>
      <div class="wpb_column vc_column_container col-sm-12 col-lg-6 col-md-8">
        <div class="vc_column-inner">
          <div class="wpb_wrapper">
            <div class="circle_id" id="6"></div>
            <div class="holderCircle">
              <div class="round"></div>
              <div class="dotCircle" style={{ transform: "rotate(288deg)", transition: "all 2s ease 0s" }}>
                <span class="itemDot itemDot0" data-tab="0"
                  style={{ left: "410px", top: "210px", transform: "rotate(72deg)", transition: "all 1s ease 0s" }}>
                  <i class="ion ion-android-settings"></i>
                  <span class="forActive"></span>
                </span>
                <span class="itemDot itemDot1" data-tab="1"
                  style={{ left: "310px", top: "383px", transform: "rotate(72deg)", transition: "all 1s ease 0s" }}>
                  <i class="ion ion-ios-chatboxes-outline"></i>
                  <span class="forActive"></span>
                </span>
                <span class="itemDot itemDot2" data-tab="2"
                  style={{ left: "110px", top: "383px", transform: "rotate(72deg)", transition: "all 1s ease 0s" }}>
                  <i class="ion ion-ios-videocam"></i>
                  <span class="forActive"></span>
                </span>
                <span class="itemDot itemDot3 active" data-tab="3"
                  style={{ left: "10px", top: "210px", transform: "rotate(72deg)", transition: "all 1s ease 0s" }}>
                  <i class="ion ion-ios-contact"></i>
                  <span class="forActive"></span>
                </span>
                <span class="itemDot itemDot4" data-tab="4"
                  style={{ left: "110px", top: "37px", transform: "rotate(72deg)", transition: "all 1s ease 0s" }}>
                  <i class="ion ion-android-clipboard"></i>
                  <span class="forActive"></span>
                </span>
                <span class="itemDot itemDot5" data-tab="5"
                  style={{ left: "310px", top: "37px", transform: "rotate(72deg)", transition: "all 1s ease 0s" }}>
                  <i class="ion ion-cube"></i>
                  <span class="forActive"></span>
                </span>
              </div>
              <div class="contentCircle">
                <div class="CirItem title-box CirItem0">
                  <h3 class="title"><span>Automation</span></h3>
                  <p>Hỗ trợ trả lời comment, ẩn comment và gửi tin
                    nhắn tự động. Giảm tối đa tỷ lệ mất và bị cướp
                    khách </p>
                  <i class="ion ion-android-settings"></i>
                </div>
                <div class="CirItem title-box CirItem1">
                  <h3 class="title"><span>Chatbot AI</span></h3>
                  <p>Hỗ trợ quản lý các đoạn hội thoại và trả lời tự
                    động bằng công nghệ AI. Giảm tối đa nguồn lực và
                    thời gian trả lời</p>
                  <i class="ion ion-ios-chatboxes-outline"></i>
                </div>
                <div class="CirItem title-box CirItem2">
                  <h3 class="title"><span>Livestream</span></h3>
                  <p>Hỗ trợ quản lý livestream và tạo đơn hàng tự
                    động. Tối ưu quy trình và giảm tối đa nguồn lực,
                    thời gian </p>
                  <i class="ion ion-ios-videocam"></i>
                </div>
                <div class="CirItem title-box CirItem3 active">
                  <h3 class="title"><span>Khách hàng</span></h3>
                  <p>Lưu trữ data khách hàng &amp; phân loại khách
                    hàng tự động, đưa ra các gợi ý về sản phẩm phù
                    hợp với khách hàng với tỷ lệ chính xác cao</p>
                  <i class="ion ion-ios-contact"></i>
                </div>
                <div class="CirItem title-box CirItem4">
                  <h3 class="title"><span>Đơn hàng</span></h3>
                  <p>Thu thập thông tin đơn hàng &amp; quản lý số
                    lượng. Tính được doanh thu và số liệu lãi - lỗ
                    cho cửa hàng </p>
                  <i class="ion ion-android-clipboard"></i>
                </div>
                <div class="CirItem title-box CirItem5">
                  <h3 class="title"><span>Sản phẩm</span></h3>
                  <p>Quản lý hàng tồn kho và số lượng xuất - nhập sản
                    phẩm. Đưa ra các gợi ý tập khách hàng phù hợp
                    với sản phẩm</p>
                  <i class="ion ion-cube"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Introduction;
