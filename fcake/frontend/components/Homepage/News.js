import { useEffect } from "react"
import { getStrapiMedia } from "lib/media";
import { formatTime } from "lib/utils";
const News = (blogData) => {
  // useEffect(() => {
  //   const scrt = document.createElement("script");
  //   const scrt1 = document.createElement("script");
  //   const scrt2 = document.createElement("script");
  //   scrt.src = "/js/owl.carousel.min.js";
  //   scrt1.src = "/js/slick.js";
  //   scrt2.src = "/js/vizion-custom.js";
  //   document.body.appendChild(scrt);
  //   document.body.appendChild(scrt1);
  //   document.body.appendChild(scrt2);
  //   return () => {
  //     document.body.removeChild(scrt)
  //     document.body.removeChild(scrt1)
  //     document.body.removeChild(scrt2)
  //   }
  // }, [])
  return (
    <div class="vc_row wpb_row vc_row-fluid news">
      <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="vc_column-inner">
          <div class="wpb_wrapper">
            <div class="title-box style-two text-center">
              <h2 class="title">Cẩm nang chia sẻ <span>kiến thức kinh
                doanh online</span></h2>
              <p class="mt-0 mb-0">Chia sẻ tài liệu, kiến thức về
                Marketing online, Quản lý và Chăm sóc khách hàng</p>
            </div>
            <div class="vizion-recentblog v1">
              <div class="owl-carousel owl-loaded owl-drag" data-dots="false" data-nav="false"
                data-items="3" data-items-laptop="3" data-items-tab="2" data-items-mobile="1"
                data-items-mobile-sm="1" data-autoplay="true" data-loop="true" data-margin="30">



                <div class="owl-stage-outer">
                  <div class="owl-stage"
                    style={{ transform: "translate3d(-1560px, 0px, 0px)", transition: "all 0.25s ease 0s", width: "3510px" }}>
                    {
                      (blogData.blogData.blog_list || []).map(blog_list => {
                        const link1 = `/blog/${blog_list?.category?.slug}/${blog_list?.slug}`;
                        return (
                          <div class="owl-item" style={{ width: "360px", marginRight: "30px" }}>
                            <div class="item">
                              <div class="iq-blog-box">
                                <div class="iq-blog-image clearfix">
                                  <img src={getStrapiMedia(blog_list?.background_image)}
                                    class="img-fluid center-block" alt="blogimage0" />
                                </div>
                                <div class="iq-blog-detail">
                                  <div class="iq-blog-meta">
                                    <ul class="list-inline">
                                      <li class="list-inline-item">
                                        <a href={link1}
                                          class="iq-user"><i class="far fa-user" aria-hidden="true"></i>
                                          datnv</a>
                                      </li>
                                      <li class="list-inline-item">
                                        <i class="far fa-calendar-minus" aria-hidden="true"></i>
                                        <span class="screen-reader-text">Posted
                                          on</span> <a
                                            href={link1}
                                            rel="bookmark"><time class="entry-date published updated"
                                              datetime="2021-06-01T08:23:08+00:00">{formatTime(blog_list?.updated_at)}</time></a>
                                      </li>
                                      <li class="list-inline-item">
                                        <a href={link1}><i
                                          class="far fa-comments" aria-hidden="true"></i>
                                          {blog_list?.comment_count || 0}</a>
                                      </li>
                                    </ul>
                                  </div>
                                  <div class="blog-title">
                                    <a href={link1}>
                                      <h6>{blog_list?.title}</h6>
                                    </a>
                                  </div>
                                  <div class="blog-button"><a class="button"
                                    href={link1}>Read
                                    More <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        )
                      })
                    }
                    {/* <div class="owl-item" style={{ width: "360px", marginRight: "30px" }}>
                      <div class="item">
                        <div class="iq-blog-box">
                          <div class="iq-blog-image clearfix">
                            <img src={getStrapiMedia(blog_list[0].background_image)}
                              class="img-fluid center-block" alt="blogimage0" />
                          </div>
                          <div class="iq-blog-detail">
                            <div class="iq-blog-meta">
                              <ul class="list-inline">
                                <li class="list-inline-item">
                                  <a href={link1}
                                    class="iq-user"><i class="far fa-user" aria-hidden="true"></i>
                                    datnv</a>
                                </li>
                                <li class="list-inline-item">
                                  <i class="far fa-calendar-minus" aria-hidden="true"></i>
                                  <span class="screen-reader-text">Posted
                                    on</span> <a
                                      href={link1}
                                      rel="bookmark"><time class="entry-date published updated"
                                        datetime="2021-06-01T08:23:08+00:00">{formatTime(blog_list[0].updated_at)}</time></a>
                                </li>
                                <li class="list-inline-item">
                                  <a href={link1}><i
                                    class="far fa-comments" aria-hidden="true"></i>
                                    {blog_list[0].comment_count && 0}</a>
                                </li>
                              </ul>
                            </div>
                            <div class="blog-title">
                              <a href={link1}>
                                <h6>{blog_list[0].title}</h6>
                              </a>
                            </div>
                            <div class="blog-button"><a class="button"
                              href={link1}>Read
                              More <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="owl-item active" style={{ width: "360px", marginRight: "30px" }}>
                      <div class="item">
                        <div class="iq-blog-box">
                          <div class="iq-blog-image clearfix">
                            <img src={getStrapiMedia(blog_list[1].background_image)}
                              class="img-fluid center-block" alt="blogimage1" />
                          </div>
                          <div class="iq-blog-detail">
                            <div class="iq-blog-meta">
                              <ul class="list-inline">
                                <li class="list-inline-item">
                                  <a href={link2}
                                    class="iq-user"><i class="far fa-user" aria-hidden="true"></i>
                                    datnv</a>
                                </li>
                                <li class="list-inline-item">
                                  <i class="far fa-calendar-minus" aria-hidden="true"></i>
                                  <span class="screen-reader-text">Posted
                                    on</span> <a
                                      href={link2}
                                      rel="bookmark"><time class="entry-date published updated"
                                        datetime="2021-06-01T05:47:52+00:00">{formatTime(blog_list[1].updated_at)}</time></a>
                                </li>
                                <li class="list-inline-item">
                                  <a
                                    href={link2}><i
                                      class="far fa-comments" aria-hidden="true"></i>
                                    {blog_list[1].comment_count && 0}</a>
                                </li>
                              </ul>
                            </div>
                            <div class="blog-title">
                              <a
                                href={link2}>
                                <h6>{blog_list[1].title}</h6>
                              </a>
                            </div>
                            <div class="blog-button"><a class="button"
                              href={link2}>Read
                              More <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="owl-item active" style={{ width: "360px", marginRight: "30px" }}>
                      <div class="item">
                        <div class="iq-blog-box">
                          <div class="iq-blog-image clearfix">
                            <img src={getStrapiMedia(blog_list[2].background_image)}
                              class="img-fluid center-block" alt="blogimage2" />
                          </div>
                          <div class="iq-blog-detail">
                            <div class="iq-blog-meta">
                              <ul class="list-inline">
                                <li class="list-inline-item">
                                  <a href={link3}
                                    class="iq-user"><i class="far fa-user" aria-hidden="true"></i>
                                    datnv</a>
                                </li>
                                <li class="list-inline-item">
                                  <i class="far fa-calendar-minus" aria-hidden="true"></i>
                                  <span class="screen-reader-text">Posted
                                    on</span> <a
                                      href={link3}
                                      rel="bookmark"><time class="entry-date published updated"
                                        datetime="2021-06-02T03:10:55+00:00">{formatTime(blog_list[2].updated_at)}</time></a>
                                </li>
                                <li class="list-inline-item">
                                  <a href={link3}><i
                                    class="far fa-comments" aria-hidden="true"></i>
                                    {blog_list[2].comment_count && 0}</a>
                                </li>
                              </ul>
                            </div>
                            <div class="blog-title">
                              <a href={link3}>
                                <h6>{blog_list[2].title}</h6>
                              </a>
                            </div>
                            <div class="blog-button"><a class="button"
                              href={link3}>Read
                              More <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div> */}
                  </div>
                </div>
                <div class="owl-nav disabled">
                  <button type="button" role="presentation" class="owl-prev">
                    <i class="fa fa-angle-right fa-2x" aria-hidden="true"></i>
                  </button>
                  <button type="button" role="presentation" class="owl-next">
                    <i class="fa fa-angle-right fa-2x"></i>
                  </button>
                </div>
                <div class="owl-dots disabled"></div>
              </div>
            </div>
            <div class="vc_row wpb_row vc_inner vc_row-fluid">
              <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                  <div class="wpb_wrapper"></div>
                </div>
              </div>
            </div>
          </div>
        </div>


      </div>
    </div>
  )
}



export default News;