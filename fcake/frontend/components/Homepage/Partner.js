import image0 from "../../assets/images/viettel-min.jpg";
import image1 from "../../assets/images/vinaphone-min.jpg";
import image2 from "../../assets/images/vtvlive-min.jpg";
import image3 from "../../assets/images/edutimes-min.jpg";
import image4 from "../../assets/images/viresa-min.jpg";
import image5 from "../../assets/images/kcloset-min.jpg";
import image6 from "../../assets/images/mobifone-min.jpg";

const Partner = () => {
  return (
    <div
      data-vc-full-width="true"
      data-vc-full-width-init="true"
      className="vc_row wpb_row vc_row-fluid vc_custom_1553859663305"
      style={{
        position: "relative",
        left: "-258.125px",
        boxSizing: "border-box",
        width: 1686,
        paddingLeft: "258.125px",
        paddingRight: "257.875px"
      }}
    >
      <div className="wpb_column vc_column_container vc_col-sm-12">
        <div className="vc_column-inner">
          <div className="wpb_wrapper">
            <div className="vc_row wpb_row vc_inner vc_row-fluid">
              <div className="wpb_column vc_column_container vc_col-sm-12">
                <div className="vc_column-inner">
                  <div className="wpb_wrapper">
                    <div className="title-box style-two text-center">
                      <h2 className="title">
                        Danh sách các đối tác của chúng tôi <span />
                      </h2>
                      <p className="mt-0 mb-0">
                        Fcake là nền tảng uy tín được nhiều đơn vị lớn tin dùng
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-sm-12">
              <div
                className="owl-carousel owl-loaded owl-drag"
                data-dots="false"
                data-nav="true"
                data-items={6}
                data-items-laptop={5}
                data-items-tab={4}
                data-items-mobile={2}
                data-items-mobile-sm={1}
                data-autoplay="true"
                data-loop="true"
                data-margin={30}
              >
                <div className="owl-stage-outer">
                  <div
                    className="owl-stage"
                    style={{
                      transform: "translate3d(-1900px, 0px, 0px)",
                      transition: "all 0.25s ease 0s",
                      width: 3610
                    }}
                  >
                    <div
                      className="owl-item"
                      style={{ width: 160, marginRight: 30 }}
                    >
                      <div className="item">
                        <div className="clients-box">
                          <img
                            className="img-fluid client-img"
                            src={image0.src}
                            alt="image0"
                          />
                        </div>
                      </div>
                    </div>

                    <div
                      className="owl-item"
                      style={{ width: 160, marginRight: 30 }}
                    >
                      <div className="item">
                        <div className="clients-box">
                          <img
                            className="img-fluid client-img"
                            src={image1.src}
                            alt="image1"
                          />
                        </div>
                      </div>
                    </div>

                    <div
                      className="owl-item"
                      style={{ width: 160, marginRight: 30 }}
                    >
                      <div className="item">
                        <div className="clients-box">
                          <img
                            className="img-fluid client-img"
                            src={image2.src}
                            alt="image2"
                          />
                        </div>
                      </div>
                    </div>

                    <div
                      className="owl-item"
                      style={{ width: 160, marginRight: 30 }}
                    >
                      <div className="item">
                        <div className="clients-box">
                          <img
                            className="img-fluid client-img"
                            src={image3.src}
                            alt="image3"
                          />
                        </div>
                      </div>
                    </div>

                    <div
                      className="owl-item"
                      style={{ width: 160, marginRight: 30 }}
                    >
                      <div className="item">
                        <div className="clients-box">
                          <img
                            className="img-fluid client-img"
                            src={image4.src}
                            alt="image4"
                          />
                        </div>
                      </div>
                    </div>

                    <div
                      className="owl-item"
                      style={{ width: 160, marginRight: 30 }}
                    >
                      <div className="item">
                        <div className="clients-box">
                          <img
                            className="img-fluid client-img"
                            src={image5.src}
                            alt="image5"
                          />
                        </div>
                      </div>
                    </div>

                    <div
                      className="owl-item"
                      style={{ width: 160, marginRight: 30 }}
                    >
                      <div className="item">
                        <div className="clients-box">
                          <img
                            className="img-fluid client-img"
                            src={image6.src}
                            alt="image6"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Partner;
