import { getStrapiURL } from "lib/api"
import { useGlobalContext } from "pages/_app"
import Icon1 from "assets/images/fcake-logo-x-300x123.png"
import Image from "next/image"
const Footer = () => {
  const { footer } = useGlobalContext()
  const topFunction = () => {
    document.body.scrollTop = 0
    document.documentElement.scrollTop = 0
  }

  return (
    <>
      <div className="footer  bg-purple d-flex justify-content-center">
        <div className="container">
          <div className="footer-top ">
            <div className="row">
              <div className="item-footer-top col-lg-4 col-md-6 col-sm-6">
                <div className="title mb-3">
                  <Image src={Icon1} alt="icon" width="100px" height="41px" />
                </div>
                <div className="content">
                  <p className=" text-white">{footer?.infomation}</p>
                </div>
              </div>
              <div className="item-footer-top col-lg-4 col-md-6 col-sm-6">
                <div className="title mb-3">
                  <Image src={Icon1} alt="icon" width="100px" height="41px" />
                </div>
                <div className="content">
                  <h4 className=" text-white">
                    Đăng ký nhận thông tin đặc biệt
                  </h4>
                  <div className="icon-contact mt-5">
                    <ul className="list-inline">
                      <li>
                        <a href="https://www.facebook.com/fcake.vn/">
                          <i className="color-purple  fab fa-facebook" />
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="item-footer-top col-lg-4 col-md-6 col-sm-6">
                <div className="title mb-3 text-white">
                  <h4>Thông tin liên hệ</h4>
                </div>
                <div className="content">
                  <ul className="text-white">
                    <li>
                      <a className="text-white" href="tel:1900636019">
                        <i className="fas fa-phone mr-2" />
                        {footer?.phone_number}
                      </a>
                    </li>
                    <li>
                      <a className="text-white" href="mailto:info@ftech.ai">
                        <i className="fas fa-envelope mr-2" />
                        {footer?.email}
                      </a>
                    </li>
                    <li></li>
                    <li>
                      <a>
                        <i className="fas fa-map-marker-alt mr-2" />{" "}
                        {footer?.address}
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div className="footer-hr">
            <hr className="pt-5" />
          </div>
          <div className="footer-bot">
            <p className="py-3 my-0 text-white">{footer?.copyright}</p>
          </div>
        </div>
      </div>
      <div id="back-to-top" onClick={() => topFunction()}>
        <a className="top" id="top">
          {" "}
          <i className="fas fa-angle-up" style={{color: "white"}} />{" "}
        </a>
      </div>
    </>
  )
}

export default Footer
