/* eslint-disable @next/next/no-html-link-for-pages */
/* eslint-disable @next/next/no-img-element */
import { memo, useState } from "react"
import { useGlobalContext } from "pages/_app"
import { useRouter } from "next/router";
import Link from "next/link"
import Image from "next/image"
import { routerConfig } from "./routerConfig"
import LOGO from "assets/images/logo-fcake.png"

const Header = () => {
  const { footer } = useGlobalContext()

  const router = useRouter();

  const [isOpen, setIsOpen] = useState(false)
  const onClickMenu = () => {
    setIsOpen((x) => !x)
  }
  return (
    <>
      <div className="header">
        <div className="sub-header d-flex justify-content-between padding-header py-2">
          <div className="sub-header-left">
            <a href="mailto:info@ftech.ai" className="mr-3">
               
              <span style={{color: 'black'}}><i className="fas fa-envelope" /></span>
            </a>
            <a href="tel:1900636019">
               
              <span style={{color: 'black'}}><i className="fas fa-phone" /></span>
            </a>
          </div>
          <div className="sub-header-right d-none d-sm-block">
            <div className="btn-use-trial">
              <a className="text-white" href="https://app.fcake.vn/">
                DÙNG THỬ MIỄN PHÍ
              </a>
            </div>
          </div>
        </div>
        {/* <div class="mobie-menu padding-header">
      </div> */}
        <div
          id="main-menu"
          className="main-menu padding-header d-flex justify-content-between align-items-center"
          style={{zIndex:'100'}}
        >
          <div className="main-menu-left">
            <Link href="/">
              <a>
                <Image src={LOGO} alt="logo-fcake" />
              </a>
            </Link>
          </div>
          <div className="main-menu-right">
            <button
              id="toggle-menu-mobile"
              className="navbar-toggler bg-purple"
              onClick={() => onClickMenu()}
            >
              <span className="navbar-toggler-icon" />
            </button>
            <div className="nav-menu">
              <ul className="ul-nav d-flex">
                {routerConfig.map((x) => (
                  <li className={router.pathname == x.link ? 'active' : ''} key={x.id}>
                    <Link href={x.link}>
                      <a>{x.title}</a>
                    </Link>
                  </li>
                ))}
              </ul>
            </div>
          </div>
          <div className={`board-menu-mobile ${isOpen && "clicked"}`}>
            <div className="list-board-menu-mobile">
              <ul>
                {routerConfig.map((x) => (
                  <li className={router.pathname == x.link ? 'active' : ''} key={x.id}>
                    <Link href={x.link}>
                      <a >{x.title}</a>
                    </Link>
                  </li>
                ))}
              </ul>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default memo(Header)
