export const routerConfig = [
  {
    id: 1,
    title: "Trang chủ",
    link: "/",
  },
  {
    id: 2,
    title: "Về chúng tôi ",
    link: "/about-us",
  },
  {
    id: 3,
    title: "Cẩm nang",
    link: "/blog",
  },
  {
    id: 4,
    title: "Liên hệ",
    link: "/contact",
  },
  {
    id: 5,
    title: "Hướng dẫn",
    link: "/blog/huong-dan",
  },
];
