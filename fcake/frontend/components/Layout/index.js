import Footer from "./Footer"
import Header from "./Header"

const Layout = ({ children }) => {
  return (
    <div className="page">
      <Header />
      {/* <Nav /> */}
      {children}
      <Footer />
    </div>
  )
}

export default Layout
