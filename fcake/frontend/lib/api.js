export function getStrapiURL(path = "") {
  return `${process.env.NEXT_PUBLIC_STRAPI_API_URL || "http://localhost:1337"
    }${path}`;
}

// Helper to make GET requests to Strapi
export async function fetchAPI(path) {
  const requestUrl = getStrapiURL(path);
  const response = await fetch(requestUrl);
  const data = await response.json();
  return data;
}
export async function postAPI(path, form) {
  const requestUrl = getStrapiURL(path);
  const response = await fetch(requestUrl, {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    method: "post",
    body: JSON.stringify(form),
  });
  const data = await response.json();
  return data;
}
export async function fetchBlogByCategory(category, page = 0) {
  const RECORD_PER_PAGE = 1;
  let path = "/blogByCategory/" + category;

  path += `?_sort=updated_at:DESC`;
  path += "&_start=" + page * RECORD_PER_PAGE;
  path += "&_limit=" + RECORD_PER_PAGE;

  return fetchAPI(path);
}
export async function fetchBlogs(page = 0) {
  const RECORD_PER_PAGE = 3;
  let path = "/blogs";

  path += `?_sort=updated_at:DESC`;
  path += "&_start=" + page * RECORD_PER_PAGE;
  path += "&_limit=" + RECORD_PER_PAGE;

  return fetchAPI(path);
}
// export async function fetchBlog() {
//   const RECORD_PER_PAGE = 1;
//   let path = "/blogs";
//   return fetchAPI(path);
// }

export async function fetchBlogByTag(tag, page = 0) {
  const RECORD_PER_PAGE = 1;
  let path = "/blogByTag/" + tag;

  path += `?_sort=updated_at:DESC`;
  path += "&_start=" + page * RECORD_PER_PAGE;
  path += "&_limit=" + RECORD_PER_PAGE;

  return fetchAPI(path);
}
export async function fetchSingleBlog(slug) {
  const path = "/blogs/" + slug;
  return fetchAPI(path);
}

export async function requestQuest(form) {
  const path = '/contact-users'
  return postAPI(path, form)
}