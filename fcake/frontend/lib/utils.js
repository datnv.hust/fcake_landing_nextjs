export const formatTime = (time, locale) => {
  var options = {
    year: "numeric",
    month: "long",
    day: "numeric",
  };

  return new Date(time).toLocaleDateString(locale || "vi", options);
};
