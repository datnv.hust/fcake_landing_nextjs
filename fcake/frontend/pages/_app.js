import App from "next/app";
import Head from "next/head";
import { createContext, useContext, useEffect } from "react";
import "assets/css/style.css";
import "assets/css/bootstrap.min.css";
import "assets/css/fontawesome.min.css";
// import "assets/css/font-awesome.css";
import "assets/css/layout.css";
import "assets/css/blog.css";
import "assets/css/animate.min.css";
import "assets/css/circle.css";
import "assets/css/ionicons.min.css";
import "assets/css/responsive.css";
import "assets/css/vizion-style.css";
import "assets/css/owl.carousel.min.css";
import "assets/css/js_composer.min.css";
import "assets/css/index.css";




import "assets/css/layout.css";
import "assets/css/blog.css";
import "assets/css/contact.css";
// import "assets/js/jquery.min.js"

// import "assets/js/banner.js"
import { fetchAPI } from "../lib/api";
import { getStrapiMedia } from "../lib/media";
import Layout from "../components/Layout";
import { ToastProvider } from 'react-toast-notifications';
// Store Strapi Global object in context
export const GlobalContext = createContext({});
export const useGlobalContext = () => useContext(GlobalContext);

const MyApp = ({ Component, pageProps }) => {
  const { global } = pageProps;

  useEffect(() => {
    const scrt = document.createElement("script");
    const scrt1 = document.createElement("script");
    const scrt2 = document.createElement("script");
    scrt.src = "/js/owl.carousel.min.js";
    scrt1.src = "/js/slick.js";
    scrt2.src = "/js/vizion-custom.js";
    document.body.appendChild(scrt);
    document.body.appendChild(scrt1);
    document.body.appendChild(scrt2);
    return () => {
      document.body.removeChild(scrt)
      document.body.removeChild(scrt1)
      document.body.removeChild(scrt2)
    }
  }, [])

  return (
    <>
      <Head>
        <link rel="shortcut icon" href={getStrapiMedia(global.favicon)} />
      </Head>
      <GlobalContext.Provider value={global}>
        <Layout>
          <ToastProvider>
            <Component {...pageProps} />
          </ToastProvider>
        </Layout>
      </GlobalContext.Provider>
    </>
  );
};

// getInitialProps disables automatic static optimization for pages that don't
// have getStaticProps. So article, category and home pages still get SSG.
// Hopefully we can replace this with getStaticProps once this issue is fixed:
// https://github.com/vercel/next.js/discussions/10949
MyApp.getInitialProps = async (ctx) => {
  // Calls page's `getInitialProps` and fills `appProps.pageProps`
  const appProps = await App.getInitialProps(ctx);
  // Fetch global site settings from Strapi
  let [global, categories, tags] = await Promise.all([
    fetchAPI("/global"),
    fetchAPI("/categories"),
    fetchAPI("/tags"),
  ]);
  global.categories = categories;
  global.tags = tags;
  // Pass the data to our page via props
  return { ...appProps, pageProps: { global } };
};

export default MyApp;
