import Document, { Html, Head, Main, NextScript } from "next/document"
class MyDocument extends Document {
  render() {
    return (
      <Html>
        <Head>
          <meta charset="utf-8" />
          <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1.0"
            id="mobile-viewport"
          />
          <title>Fcake Landingpage</title>
          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
            integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w=="
            crossorigin="anonymous" referrerpolicy="no-referrer" />
        </Head>
        <body>
          <Main />
          <FcakeScript />
          <NextScript />
        </body>
      </Html>
    )
  }
}

const FcakeScript = () => {
  return (
    <>
      <script src="/js/layout.js"></script>
      <script src="/js/bootstrap.min.js"></script>
      <script src="/js/jquery-3.2.1.slim.min.js"></script>
      <script src="/js/popper.min.js"></script>
      <script src="/js/jquery.min.js"></script>
      {/* <script src="/js/owl.carousel.min.js"></script>
      <script src="/js/vizion-custom.js"></script>
      <script src="/js/slick.js"></script> */}
    </>
  )
}
export default MyDocument
