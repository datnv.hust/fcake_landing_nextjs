import React from 'react'
import Link from "next/link"


const AboutUsSection = () => {
    return (
        <>
            <section class=" iq-breadcrumb-two text-left main-bg">
				<div class="container">
					<div class="row align-items-center">
						<div class="col-lg-6 col-sm-12">
							<div class="mb-0">
								<h2 class="title">Giới thiệu Fcake.vn</h2>
							</div>
						</div>
						<div class="col-lg-6 col-sm-12">
							<nav aria-label="breadcrumb" class="text-right">
								<ol class="breadcrumb main-bg">
									<li class="breadcrumb-item">
											<i class="fas fa-home"	aria-hidden="true"> </i>
										<Link href="/">
											Home
										</Link>
									</li>
									<li class="breadcrumb-item active">Giới thiệu Fcake.vn</li>
								</ol>
							</nav>
						</div>
					</div>
				</div>
			</section>
        </>
    )
}

export default AboutUsSection
