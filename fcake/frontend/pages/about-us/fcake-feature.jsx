import React from 'react'
import imageSearch from"../../assets/images/searching.png"
import imageChecked from"../../assets/images/checked.png"
import imagePresentation from"../../assets/images/presentation.png"

const FcakeFeature = () => {
    return (
        <>
            <div className="vc_row wpb_row vc_row-fluid vc_custom_1574403174140">
                <div className="wpb_column vc_column_container vc_col-sm-12">
                    <div className="vc_column-inner">
                        <div className="wpb_wrapper">
                            <div className="title-box text-center">
                                <h2 className="title font-weight-bold">Các tính năng của Fcake</h2>
                                <p className="sub-title"></p>
                            </div>
                            <div className="vc_row wpb_row vc_inner vc_row-fluid">
                                <div
                                    className="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-4 vc_col-md-4">
                                    <div className="vc_column-inner">
                                        <div className="wpb_wrapper">
                                            <div className="iq-insuarance-fancybox bg-show wow fadeInUp text- iq-about"
                                                data-wow-duration="0.9s"
                                                style={{/*visibility: 'hidden',*/ animationDuration: '0.9s', animationName: 'none'}}>
                                                <div className="insua-icon-bg">
                                                    {/* <i className="flaticon flaticon-search"></i> */}
                                                    <img className="img-icon" src={imageSearch.src} />
                                                </div>
                                                <div className="insua-detail">
                                                    <h5 className=" mb-1">Tự động bán hàng</h5>
                                                    <p className="mb-0">Hỗ trợ phản hồi và giảm tối
                                                        đa tỷ lệ mất khách</p>
                                                </div>
                                            </div>
                                            <ul className="iq-list wow fadeInUp"
                                                data-wow-duration="0.9s"
                                                style={{/*visibility: 'hidden',*/ animationDuration: '0.9s', animationName: 'none'}}>
                                                <li><i className="fas fa-check"></i><span>Auto reply
                                                        comment</span></li>
                                                <li><i className="fas fa-check"></i><span>Quản lý
                                                        livestream</span></li>
                                                <li><i className="fas fa-check"></i><span>Chatbot AI
                                                        tự động trả lời</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    className="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-4 vc_col-md-4">
                                    <div className="vc_column-inner">
                                        <div className="wpb_wrapper">
                                            <div className="iq-insuarance-fancybox bg-show wow fadeInUp text- iq-about"
                                                data-wow-duration="0.9s"
                                                style={{/*visibility: 'hidden',*/ animationDuration: '0.9s', animationName: 'none'}}>
                                                <div className="insua-icon-bg">
                                                    {/* <i className="flaticon flaticon-calendar"></i> */}
                                                    <img className="img-icon" src={imageChecked.src} />
                                                </div>
                                                <div className="insua-detail">
                                                    <h5 className=" mb-1">Quản lý khách hàng</h5>
                                                    <p className="mb-0">Lưu trữ data khách hàng,
                                                        phân tích hành vi khách hàng cụ thể</p>
                                                </div>
                                            </div>
                                            <ul className="iq-list wow fadeInUp"
                                                data-wow-duration="0.9s"
                                                style={{/*visibility: 'hidden',*/ animationDuration: '0.9s', animationName: 'none'}}>
                                                <li>
                                                    <i className="fas fa-check"></i>
                                                    <span>Quản lý khách hàng</span>
                                                </li>
                                                <li>
                                                    <i className="fas fa-check"></i>
                                                    <span>Fcake - Customer Profiling</span>
                                                </li>
                                                <li>
                                                    <i className="fas fa-check"></i>
                                                    <span>Fcake - Insight</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    className="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-4 vc_col-md-4">
                                    <div className="vc_column-inner">
                                        <div className="wpb_wrapper">
                                            <div className="iq-insuarance-fancybox bg-show wow fadeInUp text- iq-about"
                                                data-wow-duration="0.9s"
                                                style={{/*visibility: 'hidden',*/ animationDuration: '0.9s', animationName: 'none'}}>
                                                <div className="insua-icon-bg">
                                                    {/* <i
                                                        className="flaticon flaticon-presentation"></i> */}
                                                        <img  className="img-icon" src={imagePresentation.src} />
                                                </div>
                                                <div className="insua-detail">
                                                    <h5 className=" mb-1">Chăm sóc khách hàng</h5>
                                                    <p className="mb-0">Xây dựng chương trình và gợi
                                                        ý sản phẩm phù hợp. CSKH tự động</p>
                                                </div>
                                            </div>
                                            <ul className="iq-list wow fadeInUp"
                                                data-wow-duration="0.9s"
                                                style={{/*visibility: 'hidden',*/ animationDuration: '0.9s', animationName: 'none'}}>
                                                <li>
                                                    <i className="fas fa-check"></i>
                                                    <span>Tạo mã giảm giá &amp; Chương trình khuyến mãi</span>
                                                </li>
                                                <li>
                                                    <i className="fas fa-check"></i>
                                                    <span>Tạo gaminification</span>
                                                </li>
                                                <li>
                                                    <i className="fas fa-check"></i>
                                                    <span>Gửi tin nhắn hàng loạt</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default FcakeFeature
