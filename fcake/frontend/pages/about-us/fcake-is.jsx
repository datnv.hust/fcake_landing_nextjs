import React from 'react'

const FcakeIs = () => {
    return (
        <>
            <div
										className="vc_row wpb_row vc_row-fluid vc_custom_1574695036981 vc_row-o-equal-height vc_row-o-content-middle vc_row-flex">
										<div
											className="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
											<div className="vc_column-inner">
												<div className="wpb_wrapper">
													<div
														className="wpb_single_image wpb_content_element vc_align_left wpb_animate_when_almost_visible wpb_fadeInUp fadeInUp vc_custom_1622174238096 wpb_start_animation animated">

														<figure className="wpb_wrapper vc_figure">
															<div className="vc_single_image-wrapper vc_box_border_grey">
                                                                <img
																	width="800" height="533"
																	src="img/rsz_1rsz_1img_1756.jpg"
																	className="vc_single_image-img attachment-full" alt=""
																	loading="lazy"																	
																	sizes="100vw"/>
															</div>
														</figure>
													</div>
												</div>
											</div>
										</div>
										<div
											className="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
											<div className="vc_column-inner">
												<div className="wpb_wrapper">
													<h2 style={{textAlign: 'left'}} className="vc_custom_heading">Fcake là một
														sản phẩm của FTech CO., LTD</h2>
													<div className="wpb_text_column wpb_content_element ">
														<div className="wpb_wrapper">
															<p>FTECH là doanh nghiệp công nghệ thông tin, ứng dụng trí
																thông minh nhân tạo hàng đầu Việt Nam. Cốt lõi của một
																doanh nghiệp thành công là các sản phẩm được yêu thích
																và tin dùng bởi người dùng. Chúng tôi luôn đầu tư vào
																việc xây dựng các sản phẩm mang tính đột phá và trải
																nghiệm tốt nhất cho khách hàng. Đội ngũ của Ftech đã xây
																dựng các hệ sinh thái về Ecommerce, Edutech, Virtual
																Assistant… và các sản phẩm dịch vụ hữu ích mang tới tiện
																ích tối đa cho doanh nghiệp và khách hàng.</p>

														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
        </>
    )
}

export default FcakeIs
