import React from 'react'

const FcakePartner = () => {
    return (
        <>
            <div className="vc_row wpb_row vc_row-fluid vc_custom_1574694708002">
                <div className="wpb_column vc_column_container vc_col-sm-12">
                    <div className="vc_column-inner">
                        <div className="wpb_wrapper">
                            <div className="title-box text-center">
                                <h2 className="title font-weight-bold">Danh sách các đối tác lớn của
                                    chúng tôi</h2>
                                <p className="sub-title"></p>
                            </div>
                            <div className="col-sm-12">
                                <div className="owl-carousel owl-loaded owl-drag" data-dots="false"
                                    data-nav="true" data-items="6" data-items-laptop="3"
                                    data-items-tab="3" data-items-mobile="2"
                                    data-items-mobile-sm="1" data-autoplay="true"
                                    data-loop="true" data-margin="30">

                                    <div className="owl-stage-outer">
                                        <div className="owl-stage"
                                            style={{transform: 'translate3d(0, 0px, 0px)', transition: 'all 0.25s ease 0s', width: '3610px'}}>
                                            <div className="owl-item"
                                                style={{width: '160px', marginRight: '30px'}}>
                                                <div className="item">
                                                    <div className="clients-box">
                                                        <img className="img-fluid client-img"
                                                            src="img/viettel-min.jpg"
                                                            alt="image0"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="owl-item"
                                                style={{width: '160px', marginRight: '30px'}}>
                                                <div className="item">
                                                    <div className="clients-box">
                                                        <img className="img-fluid client-img"
                                                            src="img/vinaphone-min.jpg"
                                                            alt="image1"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="owl-item"
                                                style={{width: '160px', marginRight: '30px'}}>
                                                <div className="item">
                                                    <div className="clients-box">
                                                        <img className="img-fluid client-img"
                                                            src="img/vtvlive-min.jpg"
                                                            alt="image2"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="owl-item"
                                                style={{width: '160px', marginRight: '30px'}}>
                                                <div className="item">
                                                    <div className="clients-box">
                                                        <img className="img-fluid client-img"
                                                            src="img/edutimes-min.jpg"
                                                            alt="image3"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="owl-item"
                                                style={{width: '160px', marginRight: '30px'}}>
                                                <div className="item">
                                                    <div className="clients-box">
                                                        <img className="img-fluid client-img"
                                                            src="img/viresa-min.jpg"
                                                            alt="image4"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="owl-item"
                                                style={{width: '160px', marginRight: '30px'}}>
                                                <div className="item">
                                                    <div className="clients-box">
                                                        <img className="img-fluid client-img"
                                                            src="img/kcloset-min.jpg"
                                                            alt="image5"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="owl-item"
                                                style={{width: '160px', marginRight: '30px'}}>
                                                <div className="item">
                                                    <div className="clients-box">
                                                        <img className="img-fluid client-img"
                                                            src="img/mobifone-min.jpg"
                                                            alt="image6"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="owl-nav"><button type="button"
                                            role="presentation" className="owl-prev"><i
                                                className="fa fa-angle-left fa-2x"></i></button><button
                                            type="button" role="presentation"
                                            className="owl-next"><i
                                                className="fa fa-angle-right fa-2x"></i></button>
                                    </div>
                                    <div className="owl-dots disabled"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default FcakePartner
