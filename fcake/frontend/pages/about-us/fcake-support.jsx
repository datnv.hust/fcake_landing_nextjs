import React from 'react'

const FcakeSupport = () => {
    return (
        <>
            <div data-vc-full-width="true" data-vc-full-width-init="true"
                className="vc_row wpb_row vc_row-fluid vc_custom_1574658532353 vc_row-has-fill"
                style={{
                    position: 'relative',
                    left: '-890px',
                    boxSizing: 'border-box',
                    width: '2950px',
                    paddingLeft: '890px',
                    paddingRight: '890px',										
                }}>
                <div className="wpb_column vc_column_container vc_col-sm-12">
                    <div className="vc_column-inner">
                        <div className="wpb_wrapper">
                            <div
                                className="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1574658768535">
                                <div className="wpb_column vc_column_container vc_col-sm-12">
                                    <div className="vc_column-inner">
                                        <div className="wpb_wrapper"></div>
                                    </div>
                                </div>
                            </div>
                            <div
                                className="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-equal-height vc_row-o-content-middle vc_row-flex">
                                <div
                                    className="mb-4 mb-lg-0 wpb_column vc_column_container vc_col-sm-12 vc_col-lg-5 vc_col-md-5">
                                    <div className="vc_column-inner">
                                        <div className="wpb_wrapper">
                                            <h2 style={{textAlign: 'left'}}
                                                className="vc_custom_heading">Fcake - Nền tảng hỗ
                                                trợ bán hàng thông minh</h2>
                                            <div className="wpb_text_column wpb_content_element ">
                                                <div className="wpb_wrapper">
                                                    <p>Fcake là nền tảng hỗ trợ vận hành các
                                                        doanh nghiệp nhỏ tham gia thương mại
                                                        điện tử. Với việc áp dụng AI, công tác
                                                        tư vấn bán hàng và chăm sóc khách hàng
                                                        được nâng lên một mức độ mới: hiệu quả
                                                        hơn và tiết kiệm chi phí hơn. Các tính
                                                        năng BI cũng mang lại một góc nhìn tổng
                                                        thể và rõ ràng hơn về sức khỏe của doanh
                                                        nghiệp và biến động thị trường trong
                                                        mảng lĩnh vực kinh doanh tương ứng.</p>
                                                    <p>Chúng tôi nhắm vào các doanh nghiệp nhỏ,
                                                        bản hàng qua các nền tảng thương mại
                                                        điện tử: shopee, tiki, lazada, sendo và
                                                        các kênh mạng xã hội: facebook,
                                                        instagram, tiktok, zalo.</p>

                                                </div>
                                            </div>

                                            <div className="iq-health-progbar text-left ">
                                                <div className="progressbar-content">
                                                    <h6>Tự động giải quyết vấn đề khách hàng
                                                    </h6>
                                                    <span className="progress-value">80%</span>
                                                    <div className="iq-progress-bar">
                                                        <span data-percent="80"
                                                            style={{transition: 'width 2s ease 0s', width: '80%'}}></span>
                                                    </div>
                                                </div>
                                                <div className="progressbar-content">
                                                    <h6>Quản lý và phân tích hàng vi khách hàng
                                                    </h6>
                                                    <span className="progress-value">85%</span>
                                                    <div className="iq-progress-bar">
                                                        <span data-percent="85"
                                                            style={{																				
                                                                transition: 'width 2s ease 0s', width: '85%'
                                                            }}>																							
                                                        </span>
                                                    </div>
                                                </div>
                                                <div className="progressbar-content">
                                                    <h6>Gia tăng tỷ lệ chuyển đổi từ khách hàng
                                                        tiềm năng</h6>
                                                    <span className="progress-value">90%</span>
                                                    <div className="iq-progress-bar">
                                                        <span data-percent="90"
                                                            style={{transition: 'width 2s ease 0s', width: '90%'}}></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    className="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-7 vc_col-md-7">
                                    <div className="vc_column-inner">
                                        <div className="wpb_wrapper">
                                            <div
                                                className="wpb_single_image wpb_content_element vc_align_right vc_custom_1574403312298">

                                                <figure className="wpb_wrapper vc_figure">
                                                    <div
                                                        className="vc_single_image-wrapper vc_box_border_grey">
                                                    </div>
                                                </figure>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default FcakeSupport
