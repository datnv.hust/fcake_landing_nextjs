// import Footer from '../../components/Layout/Footer'
import React from 'react'

import AboutUsSection from './about-us-section';
import FcakeIs from './fcake-is';
import FcakeSupport from './fcake-support';
import FcakeFeature from './fcake-feature';
import FcakePartner from './fcake-partner';
import Partner from "components/Homepage/Partner"



const AboutUs = () => {
    return (
        <>
			<AboutUsSection />

            <div id="content" className="site-content">
				<div id="primary" className="content-area">
					<div id="main" className="site-main">
						<div className="container">

							<div id="post-142" className="post-142 page type-page status-publish hentry">
								<div className="sf-content">
									<FcakeIs />
									<FcakeSupport />
									<div className="vc_row-full-width vc_clearfix"></div>
									<FcakeFeature />
									{/* <FcakePartner /> */}
                  <Partner />
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>			
		</>
    )
}

export default AboutUs
