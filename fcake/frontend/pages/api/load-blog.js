import { fetchBlogByCategory, fetchBlogByTag, fetchBlogs } from "lib/api";

export default async (req, res) => {
  const { page, category, tag } = req.query;
  let fetchFuction = fetchBlogs(page);
  if (category) {
    fetchFuction = fetchBlogByCategory(category, page);
  }
  if (tag) {
    fetchFuction = fetchBlogByTag(tag, page);
  }

  const post = await fetchFuction;
  if (!post) {
    return res.status(401).json({ message: "Loading data blog fail" });
  }
  return res.status(200).json({ data: post?.blog_list });
};
