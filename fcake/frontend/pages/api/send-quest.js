import { requestQuest } from "lib/api";

export default async (req, res) => {

  let fetchFuction = requestQuest(req.body);

  const post = await fetchFuction;
  if (!post) {
    return res.status(400).json({ message: "Fail request" });
  }
  return res.status(200).json({ data: post });
};
