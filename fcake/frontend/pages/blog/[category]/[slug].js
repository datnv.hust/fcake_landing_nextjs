import React from "react";
import DetailBlog from "components/DetailBlog";
import { fetchBlogByCategory, fetchSingleBlog } from "lib/api";
const Detail = ({ detailData, blogNewUpdate }) => {
  return (
    <DetailBlog detailData={detailData || {}} blogNewUpdate={blogNewUpdate} />
  );
};
export async function getServerSideProps(context) {
  // Run API calls in parallel
  const slug = context.query.slug;
  const category = context.query.category;
  let [detailData, blogNewUpdate] = await Promise.all([
    fetchSingleBlog(slug),
    fetchBlogByCategory(category),
  ]);
  return {
    props: { detailData, blogNewUpdate },
  };
}

export default Detail;
