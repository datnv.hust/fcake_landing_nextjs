import BlogComponent from "components/Blog";
import { fetchBlogByCategory } from "lib/api";
import React from "react";

const BlogByCategory = ({ blogData }) => {
  const categoryName = blogData?.category?.name;
  const path = [
    { id: 1, name: "Blog", router: "/blog", isActive: false },
    { id: 2, name: `Archive by category : "${categoryName}"`, isActive: true },
  ];
  return (
    <BlogComponent
      data={blogData?.blog_list || []}
      title={`Category : ${categoryName}`}
      path={path}
      total={blogData?.total}
    />
  );
};
export async function getServerSideProps(context) {
  // Run API calls in parallel
  const category = context.query.category;
  let [blogData] = await Promise.all([fetchBlogByCategory(category)]);
  return {
    props: { blogData },
  };
}

export default BlogByCategory;
