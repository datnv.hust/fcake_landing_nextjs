import BlogComponent from "components/Blog";
import { fetchBlogByTag, fetchBlogs } from "lib/api";
import React from "react";

const Blog = ({ blogData, tags }) => {
  let title = "Blogs";
  let path = [
    {
      id: 1,
      name: "Blog",
      router: "/blog",
      isActive: blogData?.tag ? false : true,
    },
  ];
  const tagName = blogData?.tag?.name;
  if (tagName) {
    path.push({ id: 2, name: `Posts tagged "${tagName}"`, isActive: true });
    title = `Tag: ${tagName}`;
  }
  return (
    <BlogComponent
      key={tags ? "blog" : tags}
      data={blogData?.blog_list}
      title={title}
      path={path}
      total={blogData?.total}
    />
  );
};
export async function getServerSideProps(context) {
  // Run API calls in parallel
  const tags = context.query.tags || "";
  const fetch = tags ? fetchBlogByTag(tags) : fetchBlogs();
  let [blogData] = await Promise.all([fetch]);
  return {
    props: { blogData, tags },
  };
}

export default Blog;
