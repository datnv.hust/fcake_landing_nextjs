import { post1Async } from 'lib/request';
import React, { useState } from 'react'
import { useToasts } from 'react-toast-notifications';

const Contact = () => {
  const [form, setForm] = useState({
    username: '',
    email: '',
    title: '',
    content: ''
  })
  const { username, email, title, content } = form
  const [validField, setValidField] = useState({
    usernameValid: '',
    emailValid: ''
  })
  const { addToast, removeToast } = useToasts();
  const validEmail = (email) => {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  const changeInput = (e) => {
    const { name, value } = e.target
    setForm({
      ...form,
      [name]: value
    })
    if (name === 'username') {
      setValidField({
        ...validField,
        usernameValid: ''
      })
    }
    if (name === 'email') {
      setValidField({
        ...validField,
        emailValid: ''
      })
    }
  }
  const handleSubmit = async (e) => {
    e.preventDefault()
    if (!username) {
      setValidField({
        ...validField,
        usernameValid: 'Bạn cần điền họ và tên.'
      })
      return
    }
    if (!email) {
      setValidField({
        ...validField,
        emailValid: 'Bạn cần điền email.'
      })
      return
    } else {
      if (!validEmail(email)) {
        setValidField({
          ...validField,
          emailValid: 'Định dạng Email không hợp lệ'
        })
        return
      }
    }
    const dataLoading = await post1Async("/api/send-quest", {
      username: username,
      email: email,
      title: title,
      content: content,
    });
    setForm({
      username: '',
      email: '',
      title: '',
      content: ''
    })
    if (dataLoading.data.data) {
      addToast('Gửi thành công', { appearance: 'success', autoDismiss: true, autoDismissTimeout: 3000 })
    }
  }

  return (
    <>

      <div className="ld-breadcrumb bg-purple d-flex align-items-center text-white ">
        <div className="row mx-auto container">
          <div className="ld-breadcrumb-title col-sm-6">
            <h2>Liên hệ</h2>
          </div>

          <div className="ld-breadcrumb-path d-sm-flex justify-content-end align-items-center col-8 col-sm-6">
            <ul className="mt-sm-0 mt-3">
              <li>
                <a href="./">
                  <i className="fa fa-home" aria-hidden="true"></i>
                  Home
                </a>

              </li>
              <li className="active">
                Liên hệ
              </li>
            </ul>
          </div>

        </div>
      </div>

      <div>
        <div className="wrapper-page">
          <div className='container'>
            <div className="main-body">
              <div className="infomation-left">
                <h2>Thông tin cần hỗ trợ</h2>
                <div className="infomation-input">
                  <p>Để lại thông tin của bạn và nội dung cần hỗ trợ. Tư vấn viên Fcake sẽ liên hệ lại
                    ngay với bạn</p>
                  <form onSubmit={handleSubmit}>
                    <div className="name-email margin-info">
                      <div style={{ width: '100%' }}>
                        <p>Họ và tên (Bắt buộc)</p>
                        <input type="text" className="input input-name" name='username' value={username} onChange={changeInput} />
                        {validField.usernameValid && <p style={{ color: 'red' }}>{validField.usernameValid}</p>}
                      </div>

                      <div className="Email" style={{ width: '100%' }}>
                        <p>Email (Bắt buộc)</p>
                        <input type="text" className="input input-name" name='email' value={email} onChange={changeInput} />
                        {validField.emailValid && <p style={{ color: 'red' }}>{validField.emailValid}</p>}
                      </div>
                    </div>
                    <div style={{ width: '100%' }} className="margin-info">
                      <p>Tiêu đề</p>
                      <input type="text" className="input" style={{ width: '100%' }} name='title' value={title} onChange={changeInput} />
                    </div>
                    <div className="margin-info">
                      <p>Nội dung tin nhắn</p>
                      <textarea id="textarea" name='content' value={content} onChange={changeInput}></textarea>
                    </div>
                    <button type='submit' className="submit">GỬI</button>
                  </form>
                </div>
              </div>
              <div className="infomation-right">
                <div className="content-info">
                  <h2>Thông tin liên hệ</h2>
                  <p>
                    <a href="tel:1900636019"><i className="fas fa-phone"></i>1900636019</a>
                  </p>
                  <p>
                    <a href="mailto:info@ftech.ai"><i className="fas fa-envelope"></i>info@ftech.ai</a>
                  </p>
                  <div className="mb-4"><i className="fas fa-map-marker-alt"></i> - Chi nhánh Hà Nội: Tầng 5, số 434
                    Trần Khát Chân, Phố Huế,
                    Hai Bà Trưng, Hà Nội, Việt Nam - Chi nhánh Đà Nẵng: Tầng 11,
                    Thành Lợi 2, số 3 Lê Đình Lý, Vĩnh Trung, Thanh Khê, Đà Nẵng
                  </div>
                  <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.577621774309!2d105.85289691447575!3d21.00956178600876!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab906e04a829%3A0x8bf9545f0edf662!2sFTECH%20CO.%2C%20LTD!5e0!3m2!1svi!2s!4v1626946697672!5m2!1svi!2s"
                    style={{ border: '0' }} allowfullscreen="" loading="lazy"></iframe>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </>
  )
}
export default Contact