import Banner from "components/Homepage/Banner"
import Introduction from "components/Homepage/Introduction"
import Content from "components/Homepage/Content"
import React from "react"
import Seo from "../components/seo"
import { fetchAPI, fetchBlogs } from "../lib/api"
import News from "components/Homepage/News"
import Partner from "components/Homepage/Partner"

const Home = ({ homepage, blogData }) => {
  return (
    <>
      <Seo seo={homepage.seo} />
      <div className="uk-section">
        <Banner />
        <div className="container">
          <Introduction />
          <Content />
          <News blogData={blogData} />
          <Partner />
        </div>
      </div>
    </>
  )
}

export async function getStaticProps() {
  // Run API calls in parallel
  const [homepage] = await Promise.all([fetchAPI("/homepage")])
  const [blogData] = await Promise.all([fetchBlogs()])
  return {
    props: { homepage, blogData },
    revalidate: 1,
  }
}


export default Home
