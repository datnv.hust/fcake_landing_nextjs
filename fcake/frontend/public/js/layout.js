// fix cứng cho thanh menu ở đầu trang
window.onscroll = function () {
  myFunction()
}

function myFunction() {
  const mainMenu = document.getElementById("main-menu")
  const sticky = mainMenu.offsetTop
  const iconScrollTop = document.getElementById("back-to-top")
  iconScrollTop.style.display = "none"
  if (window.pageYOffset > sticky) {
    if (!mainMenu.classList.contains("sticky")) {
      mainMenu.classList.add("sticky")
    }
  } else {
    mainMenu.classList.remove("sticky")
  }

  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    iconScrollTop.classList.add("show-scroll-to-top")
  } else {
    iconScrollTop.classList.remove("show-scroll-to-top")
  }
}

// đóng menu mobile khi màn to
window.onresize = reportWindowSize
function reportWindowSize() {
  let width = window.innerWidth
  if (width > 1024)
    document.querySelector(".board-menu-mobile").classList.remove("clicked")
}
